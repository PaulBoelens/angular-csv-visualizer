import { Component, OnInit } from '@angular/core';
import { Issue } from '../issue';
import { IssuesService } from '../issues.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  constructor(private issuesService: IssuesService) { }

  ngOnInit() {
  }

  uploadFile(file: any) {
    const reader = new FileReader();
    reader.onload = (e: any) => {
      // Split the string up where the new line starts
      const result = e.target.result
        .split(/\r|\n|\rn/g)
        .filter(o => o);

      const issues = result.reduce((collection: Issue[], s, index) => {
          if (index === 0) {
            // Use the values that come after the headers to create the issues
            return collection;
          }

          const values = s.split(',').map(value => value.replace(/^"|"$/g, ''));

          return [
            ...collection,
            {
              firstname: values[0],
              surname: values[1],
              count: +values[2],
              dateOfBirth: this.getDate(new Date(values[3])),
            }
          ];
        }, []);

      this.issuesService.changeIssues(issues);

      // // Recalculate the max range since the issues collection changed
      // this.calculateMaxRange();

      // // Reset the filteredItems
      // this.filteredIssues = undefined;

      // // Reset the range
      // this.range = 0;
    };
    reader.readAsText(file.srcElement.files[0]);
  }

  getDate(date: Date) {
    const month = date.getUTCMonth() + 1;
    const day = date.getUTCDate();
    const year = date.getUTCFullYear();
    return `${day}-${month}-${year}`;
  }
}
