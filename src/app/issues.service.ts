import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Issue } from './issue';

@Injectable({
  providedIn: 'root'
})
export class IssuesService {
  private issuesSource = new BehaviorSubject(undefined);
  currentIssues = this.issuesSource.asObservable();

  constructor() { }

  changeIssues(issues: Issue[]) {
      this.issuesSource.next(issues);
  }
}
