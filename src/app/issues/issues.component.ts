import { Component, OnInit } from '@angular/core';
import { Issue } from '../issue';
import { IssuesService } from '../issues.service';

@Component({
  selector: 'app-issues',
  templateUrl: './issues.component.html',
  styleUrls: ['./issues.component.scss']
})

export class IssuesComponent implements OnInit {
  issues: Issue[];
  filteredIssues: Issue[];
  maxRange: number;
  
  range = 0;

  constructor(private issuesService: IssuesService) { }

  ngOnInit() {
    this.issuesService.currentIssues.subscribe(issues => {
      if (issues) {
        this.issues = issues;

        // Reset the range to 0 because the issues is updated
        this.range = 0;

        // Clear the filterd issues because the issues is updated
        this.filteredIssues = undefined;

        // Change the max range according to max count available in the issues
        this.maxRange = this.issues.reduce((max, issue) => Math.max(max, issue.count), 0);
      }
    });
  }

  changeRange(range: number) {
      this.filteredIssues = this.issues.filter(issue => issue.count >= range);
  }
}
